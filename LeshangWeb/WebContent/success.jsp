<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Jump to</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
</head>
<body>
<%
if (session.getAttribute("username")==null)
{
 out.print("<script language='javascript'>alert('Please log in!');window.location='login.jsp';</script>");
 return;
}
String username = (String)session.getAttribute("username");
int power = Integer.parseInt((String)session.getAttribute("power")) ;
String usertype="";
if(power>0) 
	{usertype="Administrator";}
else {usertype="User";}
%>
<div align="center">
Login successful! <br />
   <font color="blue">User Info:</font>
   <table border =1 >
    <tr>
     <td>&nbsp;Username:&nbsp;</td>
     <td>&nbsp;&nbsp;<%=username %>&nbsp;&nbsp;</td>
    </tr>
    <tr>
     <td>&nbsp;UserType:&nbsp;</td>
     <td>&nbsp;&nbsp;<%=usertype %>&nbsp;&nbsp;</td>
    </tr>

   </table>
   <div>
   <a href="index.jsp">Search</a>
   </div>
   <div>
   <a href="upload.jsp">Upload</a>
   </div>
   <div><a href="logout.jsp">Log Out</a></div>


</div>
</body>
</html>