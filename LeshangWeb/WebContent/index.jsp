<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>LeshangWeb</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
</head>
<body>
<%
if (session.getAttribute("username")==null)
{
 out.print("<script language='javascript'>alert('Please log in!');window.location='login.jsp';</script>");
 return;
}
%>
	<nav class="navbar navbar-default">
      <div class="container-fluid">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Welcome to DandelinWind Search!</a>
        </div>
        
      </div>
    </nav>
	
	<div class="container">
        <div class="row">
        	<div class="col-md-4 col-md-offset-4">
	    		<form action="LeshangWebServlet"> 
					<div class="form-group">
	                   <label for="origin">Please enter keywords for search.</label>
	                   <input type="text" name="resource" size="20px">
	                </div>			
					 
					<div class="form-group">
	                    <button type="submit" class="btn btn-default" id="submit">Submit</button>
	                </div>				 
				</form> 
			</div>
        </div>
     </div>	
     
     <div style="position: bottom;">
         <ul>
             <li><h6>Work by Leshang Chen</h6></li>
             <li><h6><a href="success.jsp">My Account</a></h6></li>       
         </ul>
 	</div>
	
</body>
</html>