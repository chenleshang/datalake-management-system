<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Login</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
</head>
<body>
<%
if(session.getAttribute("username") != null) response.sendRedirect("index.jsp") ;
%>
 	<form method="POST" name="frmLogin" action="LoginTestServlet">
 		<h1 align="center">User Login</h1>
 		<center>
 			<table border="1">
 				<tr>
 					<td>Username</td>
 					<td>
 					    <input type="text" name="username" value="" size="20" maxlength="20" onfocus="if (this.value=='Your name')  this.value='';" />
 					</td>
 				</tr>
 				<tr>
 					<td>Password</td>
 					<td>
 						<input type="password" name="password" value="" size="20" maxlength="20" onfocus="if(this.value=='Your Password') this.value='';">
 					</td>
 				</tr>
 				<tr>
 				 	<td>
 					    <input type="reset" name="Reset" value="Reset" />
 					</td>
 					<td>
 						<input type="submit" name="Submit" value="Submit" onclick="return validateLogin()">
 					</td>
 				</tr>
 			</table>
 			
 		</center>
 	</form>
 	
 	<div style="position: bottom;">
         <ul>
             <li><h6>Work by Leshang Chen</h6></li>     
         </ul>
 	</div>
 	
 	<script type="text/javascript">
 			   function validateLogin(){
 				 var sUserName = document.frmLogin.username.value;
 				 var sPassword = document.frmLogin.password.value;
 			    if (sUserName ==""){
 			     alert("Please Enter Username!");
 			     return false ;
 			    }
 			    
 			    if (sPassword ==""){
 			     alert("Please Enter Password!");
 			     return false ;
 			    }
 		}
 	</script>
</body>
</html>