<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
  <head>
    <title>File List</title>
    
  </head>
  
  <body>
  <%
if (session.getAttribute("username")==null)
{
 out.print("<script language='javascript'>alert('Please log in!');window.location='login.jsp';</script>");
 return;
}
%>
      <!-- 遍历Map集合 -->
    <c:forEach var="me" items="${fileNameMap}">
        <c:url value="/DownLoadServlet" var="downurl">
            <c:param name="filename" value="${me.key}"></c:param>
            <c:param name="operation" value="download"></c:param>
        </c:url>
        ${me.value}&nbsp<a href="${downurl}">Download</a>
        <c:if test="${power > 0}">
	        <c:url value="/DownLoadServlet" var="downurl">
	            <c:param name="filename" value="${me.key}"></c:param>
	            <c:param name="operation" value="delete"></c:param>
	        </c:url>
	        <a href="${downurl}">Delete</a>
		</c:if>

        <br/>
    </c:forEach>
    
   <div>
   <a href="upload.jsp">BACK</a>
   </div>
  </body>
</html>