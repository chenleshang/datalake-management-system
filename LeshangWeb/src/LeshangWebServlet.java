import java.io.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import tikaExt.MainEntrance;
import tikaExt.MongoUploader;


/**
 * Servlet implementation class LeshangWeb
 */
//@WebServlet("/LeshangWeb")
public class LeshangWebServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LeshangWebServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		String resource= request.getParameter("resource");
		response.setContentType("text/html");
	//	response.getWriter().print(resource);

		HttpSession session = request.getSession();
		if(session.getAttribute("username") == null) 
		{
			response.sendRedirect("login.jsp");
			return;
		}
		
	//	response.setHeader("Refresh", "5;URL=/LeshangWeb/ParseResult.jsp");
	//	String tempStr = tikaExt.MainEntrance.getParseResultinThisDir(this.getServletContext().getRealPath("/"));
	//	String tempStr = MongoUploader.getParsedDataFromDB();
		String tempStr = "";
		try {
			tempStr = search.Search.readData(resource, 10);//purpose testdoc.docx
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tempStr = "";
		}
		
		
		System.out.println(tempStr);
		{
			File file = new File("test.txt");
			if (file.exists()) {  
                System.out.println("Exist");  
            } else {  
                System.out.println("Not Exist");  
                file.createNewFile();
            } 
			BufferedWriter output = new BufferedWriter(new FileWriter(file));  
            output.write("Only for test use.");  
            output.close(); 
		}
		session.setAttribute("ParseResult", tempStr);
	//	response.getWriter().println(tempStr);
	//	response.getWriter().println("\r\n <div><a href=\"index.jsp\">BACK</a></div>" );
		
	//	System.out.println(request.getSession().getServletContext().getRealPath(request.getRequestURI()));
		System.out.println(this.getServletContext().getRealPath("."));
		response.sendRedirect("ParseResult.jsp");
		
		
	//	response.sendRedirect("ParseResult.jsp");
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	
	public static void main (String args[])
	{
		return;
	}

}
