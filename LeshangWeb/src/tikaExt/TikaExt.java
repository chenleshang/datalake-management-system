package tikaExt;

import java.io.File;

import java.io.IOException;
import java.util.*;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.json.*;


/**
 * TikaExt v0.4
 * The Class TikaExt is no longer the main entrance of the project. 
 * MainEntrance now contains the main function and interface. 
 * @author leshang
 *
 */
public class TikaExt 
{
	// old models
	Map<Integer, Map<String, String>> fileContents;
	Map<String, List<Map<String, Integer>>> fileLinker;
	List<Integer> examinedFiles;
	Map<String, Map<Integer, Integer>> wordCount;
	Map<String, Map<Integer, List<Integer>>> invertedIndex;
	
	// new models
	int startIndex;
	int lastIndex;
	Map<Integer, Map<String, String>> kvPairs;
	Map<String, List<Integer>> invertedIndexPairs;
	List<Map<String, Integer>> edgesInArticle;

	public TikaExt(String[] filePaths, int startIndex)
	{
		this.startIndex = startIndex;
		fileContents = new HashMap<Integer, Map<String, String>> ();
		fileLinker = new HashMap<String, List<Map<String, Integer>>>();
		Map<String, String> tempFileObj;
		for(int i = 0; i < filePaths.length; i ++)
		{
			tempFileObj = new HashMap<String, String>();
			String tempPath = filePaths[i];
			tempPath = tempPath.trim();
			String tempName = tempPath.substring(tempPath.lastIndexOf("\\")+1);
			String tempContent = getFileContent(tempPath);
			tempFileObj.put("name", tempName);
			tempFileObj.put("content", tempContent);
			fileContents.put(i, tempFileObj);
		}
		//TODO:
		generateFileLinkers();
		kvPairs = generateKVPair();
		generateInvertedIndex();
	//	System.out.println(wordCount);
	}

	//Only one main function can exist
/*
	public static void main(String[] args)
  
	{
		String[] tempPaths = {"testDoc.docx", "testDoc2.docx", "testDoc3.docx"};
		TikaExt readFiles = new TikaExt(tempPaths, 0);
	//	System.out.println(readFiles.getFileContents());
	//	System.out.println(readFiles.getFileLinkers());
		System.out.println(readFiles.getKVPair());
		System.out.println(readFiles.getEdges());
		System.out.println(readFiles.getInvertedIndex());
		
	//	String pdfPath = "cv.pdf";
	//	Tika tika = new Tika();  
		// Parse all given files and print out the extracted text content  
	//	try
	//	{
	//		String text = tika.parseToString(new File(pdfPath));
	//		text = text.toLowerCase();
		//	System.out.println(text);
	//	}
	//	catch(Exception e){}

	}*/
	
	public String getFileContent(String filePath)
	{
		// Create a Tika instance with the default configuration  
		Tika tika = new Tika();  
		// Parse all given files and print out the extracted text content  
		try
		{
			String text = tika.parseToString(new File(filePath)); 
			return text;
		}
		catch(Exception e)
		{
			System.out.println("Failed in Reading Files!");
			return null;
		}
	}
	
	public String getFileContents() throws JSONException
	{
		JSONObject tempObj = new JSONObject();
		tempObj.put("files", fileContents);
		return tempObj.toString();
	}
	
	
	public Map<Integer, Map<String, String>> generateKVPair()
	{
		int index = startIndex;
		Map<Integer, Map<String, String>> kvPair = 
				new HashMap<Integer, Map<String, String>>();
		edgesInArticle = new ArrayList<Map<String, Integer>>();
		for(int key: fileContents.keySet())
		{
			List<String> examinedWords = new ArrayList<String>();
			String tempContent = fileContents.get(key).get("content");
			//TODO: lower case
	//		tempContent = tempContent.toLowerCase();
			tempContent = tempContent.replace("\\s*|\t|\r|\n", " ");
			tempContent = tempContent.replaceAll("[^a-zA-Z]", " ");  
			String[] tempSplit = tempContent.split(" ");
			
			Map<Integer, Map<String, String>> thisNode;
			Map<String, String> thisKVPair; 

			String fileName = fileContents.get(key).get("name");
			thisKVPair = new HashMap<String, String>();
	//		fileName = fileName.replace('.', '_');
	//		System.out.println(fileName);

			fileName = fileName.substring(fileName.lastIndexOf("_")+1);
			thisKVPair.put(fileName, "FILE");
			thisNode = new HashMap<Integer, Map<String, String>>();
			kvPair.put(index, thisKVPair);
			int thisFileIndex = index;
			index ++;// plus one after node for filename is given
			
			
			for(int i = 0; i < tempSplit.length; i ++)
			{
				String word = tempSplit[i];
				if(!word.equals(""))
				{
					thisKVPair = new HashMap<String, String>();
					thisKVPair.put(word, Integer.toString(i));
					kvPair.put(index, thisKVPair);
					Map<String, Integer> linkDestRow = new HashMap<String, Integer>();
					linkDestRow.put("destnode", index);
					linkDestRow.put("orgnode", thisFileIndex);
					// create edge for each row/column
					edgesInArticle.add(linkDestRow);
					index ++;
				}
				
			}
		}
		lastIndex = index;
		return kvPair;
	}
	
	public void generateInvertedIndex()
	{
		//{nodeNumber: {keyword: [index_list]}}
		int index = startIndex;
		invertedIndexPairs = new HashMap<String, List<Integer>>();
		List<Integer> tempIndices = new ArrayList<Integer>();
		for(int i = 0; i < kvPairs.keySet().size(); i ++)
		{
			// i is the index of node
			Map<String, String> thisNode = kvPairs.get(i + index);
			String thisWord = thisNode.keySet().toString();//only one key, that is word itself
			thisWord = thisWord.substring(1, thisWord.length() - 1);
			thisWord = thisWord.toLowerCase();
			tempIndices = invertedIndexPairs.get(thisWord);
			if(tempIndices == null) tempIndices = new ArrayList<Integer>();
			tempIndices.add(i + index);
			invertedIndexPairs.put(thisWord, tempIndices);
		}
	}
	
	public void generateLinker()
	{
		//TODO: new linker generator
	}
	
	/**
	 * old functions, useless
	 */
	public void generateFileLinkers()
	{
		int thislastIndex = 0;
		examinedFiles = new ArrayList<Integer>();
		wordCount = new HashMap<String, Map<Integer, Integer>>();
		thislastIndex += fileContents.keySet().size();
		for(int key: fileContents.keySet())
		{
			List<String> examinedWords = new ArrayList<String>();
			String tempContent = fileContents.get(key).get("content");
			tempContent = tempContent.toLowerCase();
			tempContent = tempContent.replace("\\s*|\t|\r|\n", " ");
			tempContent = tempContent.replaceAll("[^a-zA-Z]", " ");  
			String[] tempSplit = tempContent.split(" ");
			
			for(String word: tempSplit)
			{
				if(!word.equals(""))
				{
					if(!examinedWords.contains(word))
					{
						Map<Integer, Integer> singleWordCount = new HashMap<Integer, Integer>();
						int count = getCountNumber(word, tempSplit);
						List<Integer> tempWordIndex = getIndexList(word, tempSplit);
						Map<Integer, List<Integer>> tempWordIndexMapping = 
								new HashMap<Integer, List<Integer>>();//with link to parent
						tempWordIndexMapping.put(key, tempWordIndex);
						if(wordCount.containsKey(word))
							singleWordCount = wordCount.get(word);
						singleWordCount.put(key, count);
						wordCount.put(word, singleWordCount);
	//					System.out.println(word + " " + key + " " + count);
					}
					
					examinedWords.add(word);
				}
			}
			examinedFiles.add(key);
		}
		
	//	System.out.println("ready");
		Map<String, List<Map<String, String>>> linker = 
				new HashMap<String, List<Map<String, String>>>();
		for(String key: wordCount.keySet())
		{
	//		System.out.println(key);
			Map<Integer, Integer> tempCount = wordCount.get(key);
			int totalCount = 0;
			int sepCount = 0;
			for(int tempStr: tempCount.keySet())
			{
				totalCount += tempCount.get(tempStr);
	//			System.out.println(tempStr);
			}
			Set<Integer> tempSet=tempCount.keySet();
			if(tempSet.size() > 1)
			{
	//			System.out.println(key);
				Integer[] tempStrSet = {};
				tempStrSet = tempSet.toArray(tempStrSet);
	//			for(String tempStr: tempStrSet)
				{
	//				System.out.println(tempStr);
				}
				//Generating Inner Content of files and weight
				List<Map<String, Integer>> fileNameList = 
						new ArrayList<Map<String, Integer>>();
				for(int i = 0; i < tempStrSet.length; i ++)
				{
					for(int j = i + 1; j < tempStrSet.length; j ++)
					{
						Map<String, Integer> tempNode = new HashMap<String, Integer>();
						int weight = 0;
					//	weight = totalCount;
						tempNode.put("orgfile", tempStrSet[i]);
						tempNode.put("destfile", tempStrSet[j]);
						weight += tempCount.get(tempStrSet[i]);
						weight += tempCount.get(tempStrSet[j]);
						tempNode.put("weight", weight);
						fileNameList = addLinker(fileNameList, tempNode);
					//	fileNameList.add(tempNode);
					}	
				}
				
				fileLinker.put(key, fileNameList);
	//			System.out.println(totalCount);
			}
			
		}
		
	}
	
	/**
	 * old functions, useless
	 */
	public List<Map<String, Integer>> addLinker(List<Map<String, Integer>> list, Map<String, Integer> node)
	{
		int orgfile = node.get("orgfile");
		int destfile = node.get("destfile");
		boolean hasNode = false;
		for(int i = 0; i < list.size(); i ++)
		{
			Map<String, Integer> tempNode = list.get(i);
			int temporgfile = tempNode.get("orgfile");
			int tempdestfile = tempNode.get("destfile");
			if((temporgfile == orgfile) && (tempdestfile == destfile))
			{
				hasNode = true;
				int linkerWeight = tempNode.get("weight");
				linkerWeight += node.get("weight");
				list.remove(i);
				tempNode.put("weight", linkerWeight);
				list.add(tempNode);
				break;
			}
		}
		if(hasNode != true)
		{
			list.add(node);
		}
		return list;
	}
	
	public List<Integer> getIndexList(String word, String[] sentence)
	{
		List<Integer> tempIndexList = new ArrayList<Integer>();
		for(int i = 0; i < sentence.length; i++)
		{
			if(sentence[i].equals(word)) tempIndexList.add(i);
		}
		return tempIndexList;
	}
	
	public int getCountNumber(String word, String[] sentence)
	{
		int count = 0;
		for(String tempWord: sentence)
		{
			if(word.equals(tempWord)) count += 1;
		}
		return count;
	}
	
	/**
	 *  now useless function
	 * @return 
	 */
	public String getFileLinkers() throws JSONException
	{
		JSONObject fileLinkerJSON = new JSONObject();
		fileLinkerJSON.put("Linker", fileLinker);
		return fileLinkerJSON.toString();
	}
	
	public String getKVPair() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("ARTICLE_KVPAIR", kvPairs);
		return jsonKVPair.toString();
	}
	
	public String getEdges() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("ARTICLE_EDGE", edgesInArticle);
		return jsonKVPair.toString();
	}
	
	public String getInvertedIndex() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("ARTICLE_INVERT_INDEX", invertedIndexPairs);
		return jsonKVPair.toString();
	}
	
	public int getStartIndex()
	{
		return startIndex;
	}
	
	public void setStartIndex(int startIndex)
	{
		this.startIndex = startIndex;
	}
	
	public int getLastIndex()
	{
		return lastIndex;
	}
	
	public void setLastIndex(int lastIndex)
	{
		this.lastIndex = lastIndex;
	}
	
	public Map<String, List<Integer>> getInvertedIndexPairs()
	{
		return invertedIndexPairs;
	}
	
	public List<Map<String, Integer>> getEdgeList()
	{
		return edgesInArticle;
	}
	
	public Map<Integer, Map<String, String>> getKVPairInMap()
	{
		return kvPairs;
	}

 
} 