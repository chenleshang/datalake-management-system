package tikaExt;

import java.io.*;
import java.util.*;

import org.apache.commons.csv.*;
import org.json.*;

/**
 * Deals with CSV table. 
 * @author leshang
 *
 */
public class CsvExt 
{
	int startIndex;
	int lastIndex;
	Map<Integer, Map<String, String>> kvPairs;
	List<Map<String, Integer>> egdesInCSV;
	Map<String, List<Integer>> invertedIndexPairs;
	
	public CsvExt(String[] filePaths, int startIndex)
	{
		this.startIndex = startIndex;
		kvPairs = Extract(filePaths);
//		System.out.println(kvPairs);
		generateInvertedIndex();
//		System.out.println(kvPairs);
//		System.out.println(egdesInCSV);
	}
/*
	public static void main(String args[]) 
	{
		String[] paths = {"testTable2.csv","testTable1.csv"};
		CsvExt test = new CsvExt(paths, 10);
		System.out.println(test.getKVPair());
		System.out.println(test.getEdges());
		System.out.println(test.getInvertedIndex());
	}*/
	
	public Map<Integer, Map<String, String>> Extract(String[] filePaths)
	{
		int index = startIndex;
		try 
		{
			Map<Integer, Map<String, String>> kvPair = 
					new HashMap<Integer, Map<String, String>>();
			egdesInCSV = new ArrayList<Map<String, Integer>>();
			for(String filePath: filePaths)
			{
				Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(new FileReader(filePath));
				int tempCsvIndex = 0;
				List<String> headerList = new ArrayList<String>();
				Map<String, String> fileNameRow = new HashMap<String, String>();
				String modFileName =filePath.substring(filePath.lastIndexOf("\\")+1);
	//			modFileName = modFileName.replace('.', '_');
				modFileName = modFileName.substring(modFileName.lastIndexOf("_")+1);
				fileNameRow.put(modFileName, "FILE");
				kvPair.put(index, fileNameRow);
				int fileIndex = index;
				index ++;

				for (CSVRecord record : records) 
				{
					// get header
					// first extract to KV pair with node index/ row number
					// then convert to invert index
				//	System.out.println(record);
					
					if(tempCsvIndex == 0)
					{
						for(int j = 0; j < record.size(); j ++)
							headerList.add(record.get(j));
					}
					else
					{
						Map<String, String> thisRow = new HashMap<String, String>();
						thisRow.put("row", Integer.toString(tempCsvIndex));
						kvPair.put(index, thisRow);
						int tempRowIndex = index;
						
						Map<String, Integer> linkRowToFile = new HashMap<String, Integer>();
						linkRowToFile.put("destnode", tempRowIndex);
						linkRowToFile.put("orgnode", fileIndex);
						egdesInCSV.add(linkRowToFile);
						
						index ++;
						for(int j = 0; j < record.size(); j ++)
						{
							Map<String, Integer> linkDestRow = new HashMap<String, Integer>();
							Map<String, String> tempNode = new HashMap<String, String>();
							tempNode.put(headerList.get(j), record.get(j)) ;
							kvPair.put(index, tempNode);
							linkDestRow.put("destnode", index);
							linkDestRow.put("orgnode", tempRowIndex);
							// create edge for each row/column
							egdesInCSV.add(linkDestRow);
							index ++;
						}
						
					}
					
					
					
				//	System.out.println(headerList); 
					tempCsvIndex ++ ;
				}

			}
			lastIndex = index;
			return kvPair;
			
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Almost the same with Doc files. (dif: key value both should have inverted index)
	 */
	public void generateInvertedIndex()
	{
		//{nodeNumber: {keyword: [index_list]}}
		int index = startIndex;
		invertedIndexPairs = new HashMap<String, List<Integer>>();
		List<Integer> tempIndices = new ArrayList<Integer>();
		for(int i = 0; i < kvPairs.keySet().size(); i ++)
		{
			// i is the index of node
			Map<String, String> thisNode = kvPairs.get(i + index);
			String thisWord = thisNode.keySet().toString();//only one key, that is word itself
			thisWord = thisWord.substring(1, thisWord.length() - 1);
		//	System.out.println(thisWord);
			thisWord = thisWord.toLowerCase();
			tempIndices = invertedIndexPairs.get(thisWord);
			if(tempIndices == null) tempIndices = new ArrayList<Integer>();
			tempIndices.add(i + index );
			invertedIndexPairs.put(thisWord, tempIndices);
			
			//for values
			thisWord = thisNode.keySet().toString();//only one key, that is the attribute
			thisWord = thisWord.substring(1, thisWord.length() - 1);
			thisWord = thisNode.get(thisWord);	// get the value
		//	System.out.println(thisWord);
			thisWord = thisWord.toLowerCase();
			tempIndices = invertedIndexPairs.get(thisWord);
			if(tempIndices == null) tempIndices = new ArrayList<Integer>();
			tempIndices.add(i + index);
			invertedIndexPairs.put(thisWord, tempIndices);
		}
	}
	
	public String getKVPair() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("TABLE_KVPAIR", kvPairs);
		return jsonKVPair.toString();
	}
	
	public Map<Integer, Map<String, String>> getKVPairInMap()
	{
		return kvPairs;
	}
	
	public String getEdges() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("TABLE_EDGE", egdesInCSV);
		return jsonKVPair.toString();
	}
	
	public int getStartIndex()
	{
		return startIndex;
	}
	
	public void setStartIndex(int startIndex)
	{
		this.startIndex = startIndex;
	}
	
	public String getInvertedIndex() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("TABLE_INVERT_INDEX", invertedIndexPairs);
		return jsonKVPair.toString();
	}
	
	public int getLastIndex()
	{
		return lastIndex;
	}
	
	public void setLastIndex(int lastIndex)
	{
		this.lastIndex = lastIndex;
	}
	
	public Map<String, List<Integer>> getInvertedIndexPairs()
	{
		return invertedIndexPairs;
	}
	
	public List<Map<String, Integer>> getEdgeList()
	{
		return egdesInCSV;
	}

}
