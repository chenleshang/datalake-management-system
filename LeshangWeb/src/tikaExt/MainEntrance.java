package tikaExt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import org.bson.*;
import org.json.*;

/**
 * This class defines the main entrance.
 * @author leshang
 *
 */
public class MainEntrance 
{
	int startIndex;
	int lastIndex;
	CsvExt csvFilesObject;
	TikaExt articleFileObj;
	Map<Integer, Map<String, String>> kvPairs;
	Map<String, Map<String, Integer>> linker;
	Map<String, List<Integer>> totalIIP;
	List<Map<String, Integer>> allEdges;
	static int SAMESTRINGWEIGHT = 5;
	
	/**
	 * TikaExt v0.6
	 * TikaExt v0.4
	 * What's New:
	 * The Class TikaExt is no longer the main entrance of the project. 
	 * MainEntrance now contains the main function and interface. 
	 * Add generateLinker in MainEntrace.
	 * TikaExt v0.3
	 * What's New:
	 * Support CSV file format
	 * Have the following JSON Object: node, edge
	 * node has index as key and {index, word} pair as value
	 * changed format of inverted index to {word:[node numbers]}
	 * TikaExt v0.2
	 * What's New:
	 * Linker: Changed filename to integer index. 
	 * Only test of reading files and output as Strings.
	 * Also test for output Likers. 
	 * @author leshang
	 *
	 */
	public MainEntrance(String[] filePaths, int index)
	{
		startIndex = index;
		kvPairs = new HashMap<Integer, Map<String, String>> ();
		List<String> docPaths = new ArrayList<String>();
		List<String> csvPaths = new ArrayList<String>();
		for(String filePath: filePaths)
		{
			if(filePath.contains(".doc") || filePath.contains(".docx") || 
					filePath.contains(".pdf")||
					filePath.endsWith(".htm") || filePath.endsWith(".html") ||
					filePath.endsWith(".txt"))
			{
				docPaths.add(filePath);
			}
			else if(filePath.contains(".csv"))
			{
				csvPaths.add(filePath);
			}
		}
		String[] csvFiles = csvPaths.toArray(new String[0]);
		String[] docFiles = docPaths.toArray(new String[0]);
		csvFilesObject = new CsvExt(csvFiles, startIndex);
		lastIndex = csvFilesObject.getLastIndex();
	//	System.out.println(docFiles);
		articleFileObj = new TikaExt(docFiles, lastIndex);
		lastIndex = articleFileObj.getLastIndex();
		totalIIP = new HashMap<String, List<Integer>>();
		generateLinker();
		generateAllEdges();
		setKVPair();
	}
	
	public MainEntrance(String[] filePaths)
	{
		startIndex = 0;
		kvPairs = new HashMap<Integer, Map<String, String>> ();

		totalIIP = new HashMap<String, List<Integer>>();
		linker = new HashMap<String, Map<String, Integer>> ();
		allEdges = new ArrayList<Map<String, Integer>> ();
//		startIndex = index;
		updataDatabyJSONArray("[{\"INVERTED_INDEX\":{\"purpose\":[30,3,6,9,13,16,19],\"testtable1.csv\":[0],\"test no link\":[16],\"for\":[44],\"box\":[25],\"testdoc2.docx\":[31,5,18],\"good\":[38],\"life\":[21,36,42,45,46,47,48,19],\"file\":[0,2,5,8,10],\"for link to file1\":[13],\"and\":[35],\"of\":[26],\"testtable2.csv\":[10],\"have\":[40],\"testdoc1.csv\":[12],\"row\":[1,4,7,11,14,17],\"testdoc3.docx\":[39,8],\"table\":[12,15,18],\"for test use 2\":[6],\"testdoc.docx\":[20,2],\"a\":[24,41],\"for test use 3\":[9],\"for test use 1\":[3],\"test\":[43],\"like\":[23],\"this\":[28],\"is\":[22,29,33,37],\"1\":[1,11],\"2\":[4,14],\"3\":[7,17],\"sweet\":[34],\"nonlink.csv\":[15],\"chocolate\":[27,32]}},\n{\"EDGE\":[{\"destnode\":21,\"orgnode\":20},{\"destnode\":22,\"orgnode\":20},{\"destnode\":23,\"orgnode\":20},{\"destnode\":24,\"orgnode\":20},{\"destnode\":25,\"orgnode\":20},{\"destnode\":26,\"orgnode\":20},{\"destnode\":27,\"orgnode\":20},{\"destnode\":28,\"orgnode\":20},{\"destnode\":29,\"orgnode\":20},{\"destnode\":30,\"orgnode\":20},{\"destnode\":32,\"orgnode\":31},{\"destnode\":33,\"orgnode\":31},{\"destnode\":34,\"orgnode\":31},{\"destnode\":35,\"orgnode\":31},{\"destnode\":36,\"orgnode\":31},{\"destnode\":37,\"orgnode\":31},{\"destnode\":38,\"orgnode\":31},{\"destnode\":40,\"orgnode\":39},{\"destnode\":41,\"orgnode\":39},{\"destnode\":42,\"orgnode\":39},{\"destnode\":43,\"orgnode\":39},{\"destnode\":44,\"orgnode\":39},{\"destnode\":45,\"orgnode\":39},{\"destnode\":46,\"orgnode\":39},{\"destnode\":47,\"orgnode\":39},{\"destnode\":48,\"orgnode\":39},{\"destnode\":1,\"orgnode\":0},{\"destnode\":2,\"orgnode\":1},{\"destnode\":3,\"orgnode\":1},{\"destnode\":4,\"orgnode\":0},{\"destnode\":5,\"orgnode\":4},{\"destnode\":6,\"orgnode\":4},{\"destnode\":7,\"orgnode\":0},{\"destnode\":8,\"orgnode\":7},{\"destnode\":9,\"orgnode\":7},{\"destnode\":11,\"orgnode\":10},{\"destnode\":12,\"orgnode\":11},{\"destnode\":13,\"orgnode\":11},{\"destnode\":14,\"orgnode\":10},{\"destnode\":15,\"orgnode\":14},{\"destnode\":16,\"orgnode\":14},{\"destnode\":17,\"orgnode\":10},{\"destnode\":18,\"orgnode\":17},{\"destnode\":19,\"orgnode\":17}]},\n{\"KVPAIR\":{\"0\":{\"testTable1.csv\":\"FILE\"},\"1\":{\"row\":\"1\"},\"2\":{\"File\":\"testDoc.docx\"},\"3\":{\"Purpose\":\"for test use 1\"},\"4\":{\"row\":\"2\"},\"5\":{\"File\":\"testDoc2.docx\"},\"6\":{\"Purpose\":\"for test use 2\"},\"7\":{\"row\":\"3\"},\"8\":{\"File\":\"testDoc3.docx\"},\"9\":{\"Purpose\":\"for test use 3\"},\"10\":{\"testTable2.csv\":\"FILE\"},\"11\":{\"row\":\"1\"},\"12\":{\"Table\":\"testDoc1.csv\"},\"13\":{\"Purpose\":\"for link to file1\"},\"14\":{\"row\":\"2\"},\"15\":{\"Table\":\"NonLink.csv\"},\"16\":{\"Purpose\":\"test no link\"},\"17\":{\"row\":\"3\"},\"18\":{\"Table\":\"testDoc2.docx\"},\"19\":{\"Purpose\":\"life\"},\"20\":{\"testDoc.docx\":\"FILE\"},\"21\":{\"Life\":\"0\"},\"22\":{\"is\":\"1\"},\"23\":{\"like\":\"2\"},\"24\":{\"a\":\"3\"},\"25\":{\"box\":\"4\"},\"26\":{\"of\":\"5\"},\"27\":{\"chocolate\":\"6\"},\"28\":{\"This\":\"9\"},\"29\":{\"is\":\"10\"},\"30\":{\"purpose\":\"11\"},\"31\":{\"testDoc2.docx\":\"FILE\"},\"32\":{\"Chocolate\":\"0\"},\"33\":{\"is\":\"1\"},\"34\":{\"sweet\":\"2\"},\"35\":{\"and\":\"3\"},\"36\":{\"life\":\"4\"},\"37\":{\"is\":\"5\"},\"38\":{\"good\":\"6\"},\"39\":{\"testDoc3.docx\":\"FILE\"},\"40\":{\"Have\":\"0\"},\"41\":{\"a\":\"1\"},\"42\":{\"life\":\"2\"},\"43\":{\"test\":\"3\"},\"44\":{\"for\":\"4\"},\"45\":{\"life\":\"5\"},\"46\":{\"Life\":\"7\"},\"47\":{\"life\":\"8\"},\"48\":{\"life\":\"9\"}}},\n{\"LINKER\":{\"testdoc1.csv@_@testdoc.docx\":{\"weight\":1,\"destnode\":2,\"orgnode\":12},\"purpose\":{\"weight\":10,\"destnode\":19,\"orgnode\":16},\"testdoc2.docx@_@testdoc1.csv\":{\"weight\":1,\"destnode\":12,\"orgnode\":18},\"for@_@for link to file1\":{\"weight\":1,\"destnode\":13,\"orgnode\":44},\"testdoc2.docx\":{\"weight\":10,\"destnode\":18,\"orgnode\":5},\"testtable2.csv@_@table\":{\"weight\":1,\"destnode\":18,\"orgnode\":10},\"life\":{\"weight\":10,\"destnode\":19,\"orgnode\":48},\"testtable1.csv@_@table\":{\"weight\":1,\"destnode\":18,\"orgnode\":0},\"testdoc1.csv@_@testdoc3.docx\":{\"weight\":1,\"destnode\":8,\"orgnode\":12},\"testdoc2.docx@_@testdoc.docx\":{\"weight\":1,\"destnode\":2,\"orgnode\":18},\"for test use 1@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":3},\"for test use 2@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":6},\"for test use 3@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":9},\"for test use 3@_@for test use 1\":{\"weight\":1,\"destnode\":3,\"orgnode\":9},\"row\":{\"weight\":10,\"destnode\":17,\"orgnode\":14},\"testdoc3.docx\":{\"weight\":10,\"destnode\":8,\"orgnode\":39},\"testtable1.csv@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":0},\"table\":{\"weight\":10,\"destnode\":18,\"orgnode\":15},\"testdoc.docx\":{\"weight\":10,\"destnode\":2,\"orgnode\":20},\"testtable1.csv@_@testtable2.csv\":{\"weight\":1,\"destnode\":10,\"orgnode\":0},\"testdoc3.docx@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":8},\"testdoc3.docx@_@testdoc.docx\":{\"weight\":1,\"destnode\":2,\"orgnode\":8},\"test no link@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":16},\"testtable2.csv@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":10},\"for@_@for test use 1\":{\"weight\":1,\"destnode\":3,\"orgnode\":44},\"testdoc.docx@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":2},\"for@_@for test use 2\":{\"weight\":1,\"destnode\":6,\"orgnode\":44},\"for@_@for test use 3\":{\"weight\":1,\"destnode\":9,\"orgnode\":44},\"testdoc1.csv@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":12},\"for test use 2@_@for test use 1\":{\"weight\":1,\"destnode\":3,\"orgnode\":6},\"for test use 2@_@for test use 3\":{\"weight\":1,\"destnode\":9,\"orgnode\":6},\"testdoc2.docx@_@testdoc3.docx\":{\"weight\":1,\"destnode\":8,\"orgnode\":18},\"testdoc2.docx@_@test\":{\"weight\":1,\"destnode\":43,\"orgnode\":18},\"chocolate\":{\"weight\":10,\"destnode\":32,\"orgnode\":27}}},\n{\"LAST_INDEX\":49}]");
		
		List<String> docPaths = new ArrayList<String>();
		List<String> csvPaths = new ArrayList<String>();
		for(String filePath: filePaths)
		{
			if(filePath.contains(".doc") || filePath.contains(".docx") || 
					filePath.contains(".pdf")||
					filePath.endsWith(".htm") || filePath.endsWith(".html") ||
					filePath.endsWith(".txt"))
			{
				docPaths.add(filePath);
			}
			else if(filePath.contains(".csv"))
			{
				csvPaths.add(filePath);
			}
		}
		String[] csvFiles = csvPaths.toArray(new String[0]);
		String[] docFiles = docPaths.toArray(new String[0]);
		csvFilesObject = new CsvExt(csvFiles, lastIndex);
		lastIndex = csvFilesObject.getLastIndex();
	//	System.out.println(docFiles);
		articleFileObj = new TikaExt(docFiles, lastIndex);
		lastIndex = articleFileObj.getLastIndex();
		generateLinker();
		generateAllEdges();
		setKVPair();
	}
	
	public static void main(String[] args) throws IOException 
	{
		// TODO Auto-generated method stub
		
	//	String[] paths = {"testTable2.csv","testTable1.csv"};
	//	CsvExt test = new CsvExt(paths, 0);
	//	System.out.println(test.getKVPair());
	//	System.out.println(test.getEdges());
	//	System.out.println(test.getInvertedIndex());
		
	//	String[] tempPaths = {"testDoc.docx", "testDoc2.docx", "testDoc3.docx"};
	//	TikaExt readFiles = new TikaExt(tempPaths, 0);
	//	System.out.println(readFiles.getFileContents());
	//	System.out.println(readFiles.getFileLinkers());
	//	System.out.println(readFiles.getKVPair());
	//	System.out.println(readFiles.getEdges());
	//	System.out.println(readFiles.getInvertedIndex());
		
		

		
//		List<String> fileLists = new ArrayList<String>();
//		File file = new File(".");
//	//	File file = new File("/");
//		File[] lf = file.listFiles();
//		String parentPath = "";
//		String tempPath = "";
//		for(int i=0; i<lf.length; i++)
//		{
//			if(lf[i].getName().endsWith(".doc") || lf[i].getName().endsWith(".docx") ||
//					/*lf[i].getName().endsWith(".pdf") || */lf[i].getName().endsWith(".csv")) 
//			{
//				//System.out.println("Is doc!");
//				tempPath = lf[i].getCanonicalPath();
//				parentPath = tempPath.substring(0, tempPath.lastIndexOf("\\")+1);
//				fileLists.add(lf[i].getCanonicalPath());
//			}
////			System.out.println(lf[i].getCanonicalPath());
//			//System.out.println(lf[i].getName());
//		}
//		String[] paths = fileLists.toArray(new String[0]);
		
//		for(int i = 0; i < paths.length; i ++)
//		{
//			System.out.println(paths[i]);
//		}
				
		
//		String[] testpaths = {"testTable2.csv","testTable1.csv", "testDoc.docx", "testDoc2.docx", "testDoc3.docx"};
//		MainEntrance instance = new MainEntrance(paths, 0);
//		System.out.println(instance.getInvertedIndexInOne());
//		System.out.println(instance.getEdgesInOne());
//		System.out.println(instance.getKVPairInBSON());
//		System.out.println(instance.getLinker());
//		System.out.println(instance.getLastIndexInJSON());
		
	//	System.out.println(parentPath);
		
//		
//		String content = MainEntrance.getParseResultinThisDir("D:\\test\\test");
//		System.out.println(content);
//		
//		try 
//		{
//			String filePath = "test.txt";
//			File file1 = new File(filePath);
//			if (file1.exists()) 
//			{  
//	          //  System.out.println("Exist");  
//	        } 
//			else 
//	        {  
//	          //  System.out.println("Not Exist");  
//				file1.createNewFile();
//	
//	        } 
//			BufferedWriter output = new BufferedWriter(new FileWriter(file1));  
//	        
//			output.write(content);
//	        output.close(); 
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//		}
		
		List<String> fileLists = new ArrayList<String>();
		
		fileLists = getAllFileinPaths("D:\\test\\test");

		String[] paths = fileLists.toArray(new String[0]);
		MainEntrance instance = new MainEntrance(paths);
		
		String tempStr = "";
		try {
			tempStr += "[" + instance.getInvertedIndexInOne();
			tempStr += ",\n";
			tempStr += instance.getEdgesInOne();
			tempStr += ",\n";
			tempStr += instance.getKVPairs();
			tempStr += ",\n";
			tempStr += instance.getLinker();
			tempStr += ",\n";
			tempStr += instance.getLastIndexInJSON();
			tempStr += "]\n";
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(tempStr);
//		instance.updataDatabyJSONArray(MongoUploader.getParsedDataFromDB());
		

		
	//	System.out.println(getSimilarity("justification", "justify"));
	//	System.out.println(getSimilarity("examination", "exam"));
	//	System.out.println(getSimilarity(null, "justify"));
	//	System.out.println(getSimilarity("like", "dislike"));
	//	System.out.println(getSimilarity("jusfeaf", "justify"));
		
	}
	
	public static String getParseResultinThisDir(String realPath) throws IOException
	{
		List<String> fileLists = new ArrayList<String>();
		
		fileLists = getAllFileinPaths(realPath);

		String[] paths = fileLists.toArray(new String[0]);

		MainEntrance instance = new MainEntrance(paths, 0);
	//	instance.updataDatabyJSONArray(MongoUploader.getParsedDataFromDB());
		String tempStr = "";
		try {
			tempStr += "[" + instance.getInvertedIndexInOne();
			tempStr += ",\n";
			tempStr += instance.getEdgesInOne();
			tempStr += ",\n";
			tempStr += instance.getKVPairs();
			tempStr += ",\n";
			tempStr += instance.getLinker();
			tempStr += ",\n";
			tempStr += instance.getLastIndexInJSON();
			tempStr += "]\n";
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tempStr;
	}
	
	static List<String> getAllFileinPaths(String realPath) throws IOException
	{
		List<String> fileLists = new ArrayList<String>();
		File file = new File(realPath);
		File[] lf = file.listFiles();
		for(int i=0; i<lf.length; i++)
		{
			if(lf[i].isFile())
			{
				if(lf[i].getName().endsWith(".doc") || lf[i].getName().endsWith(".docx") ||
						lf[i].getName().endsWith(".pdf") || lf[i].getName().endsWith(".csv") ||
						lf[i].getName().endsWith(".htm") || lf[i].getName().endsWith(".html") ||
						lf[i].getName().endsWith(".txt")) 
				{
					//System.out.println("Is doc!");
					fileLists.add(lf[i].getCanonicalPath());
				}
				//System.out.println(lf[i].getName());
			}
			else if(lf[i].isDirectory())
			{
				fileLists.addAll(getAllFileinPaths(lf[i].getCanonicalPath()));
			}
			
		}
		return fileLists;
	}
	
	public static String getParseResultinThisDir(String[] paths)
	{
		String[] realPaths = paths;
		for(int i = 0; i < paths.length; i ++)
		{
			String tempPath = paths[i];
			realPaths[i] = tempPath;
		}
		MainEntrance instance = new MainEntrance(realPaths, 0);
		String tempStr = "";
		try {
			tempStr += instance.getInvertedIndexInOne();
			tempStr += "\n";
			tempStr += instance.getEdgesInOne();
			tempStr += "\n";
			tempStr += instance.getKVPair();
			tempStr += "\n";
			tempStr += instance.getLinker();
			tempStr += "\n";
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tempStr;
	}
	
	public void generateLinker()
	{
		linker = new HashMap<String, Map<String, Integer>>();
		Map<String, List<Integer>> articleIIP = articleFileObj.getInvertedIndexPairs();
		Map<String, List<Integer>> csvIIP = csvFilesObject.getInvertedIndexPairs();
//		System.out.println(articleIIP);
//		System.out.println(csvIIP);
//		totalIIP = new HashMap<String, List<Integer>>();
		List<Integer> tempList = new ArrayList<Integer>();
		List<Integer> tempList2 = new ArrayList<Integer>();
		List<Integer> tempList3 = new ArrayList<Integer>();
		totalIIP.putAll(articleIIP);
		// Merging while updating two Map
		for(String key: csvIIP.keySet())
		{
			tempList = new ArrayList<Integer>();
			tempList2 = new ArrayList<Integer>();
			tempList3 = new ArrayList<Integer>();
			if(totalIIP.containsKey(key))
			{
				tempList2 = totalIIP.get(key);
				tempList3 = csvIIP.get(key);
				tempList.addAll(tempList2);
				tempList.addAll(tempList3);
				totalIIP.put(key, tempList);
			}
			else
			{
				tempList = csvIIP.get(key);
				totalIIP.put(key, tempList);
			}
		}
	//	System.out.println(totalIIP);
		
		//Equal value matching
		Map<String, Integer> oneLink;
		int linkWeight = 0;
		for(String key: totalIIP.keySet())
		{
			if(!isCommonWord(key))
			{
				List<Integer> thisInvertedIndex = totalIIP.get(key);
				if(thisInvertedIndex.size() > 1)// at least 2 nodes, have linker
				{
					for(int i = 0; i < thisInvertedIndex.size(); i ++)
					{
						for(int j = i + 1; j < thisInvertedIndex.size(); j ++)
						{
							linkWeight = SAMESTRINGWEIGHT;// weight for linker of same value
							oneLink = new HashMap<String, Integer>();
							oneLink.put("orgnode", thisInvertedIndex.get(i));
							oneLink.put("destnode", thisInvertedIndex.get(j));
							oneLink.put("weight", linkWeight);
							linker.put(key/*+"_"+Integer.toString(i)+"_"+Integer.toString(j)*/, oneLink);
						}
					}
				}
			}
		}
		
		//Approximate Matching
		String[] keySetStrings = totalIIP.keySet().toArray(new String[0]);
		String keyString = "";
		String keyString2 = "";
		int similarity ;
		List<Integer> oneIIList ;
		List<Integer> anotherIIList ;
		for(int i = 0; i < keySetStrings.length; i ++)
		{
			keyString = keySetStrings[i];
			for(int j = i + 1; j < keySetStrings.length; j ++)
			{
				keyString2 = keySetStrings[j];
				similarity = getSimilarity(keyString, keyString2);
				if(similarity > 0)
				{
					oneIIList = totalIIP.get(keyString);
					anotherIIList = totalIIP.get(keyString2);
					for(int m = 0; m < oneIIList.size(); m ++)
					{
						for(int n = 0; n < anotherIIList.size(); n ++)
						{
							linkWeight = similarity;// weight for linker of same value
							oneLink = new HashMap<String, Integer>();
							oneLink.put("orgnode", oneIIList.get(m));
							oneLink.put("destnode", anotherIIList.get(n));
							oneLink.put("weight", linkWeight);
							linker.put(keyString+"@_@"+keyString2, oneLink);
						}
					}
				}
			}
		}
	}

	//If common word, return true; 
	//very simply implementation by word length and word list;
	public static boolean isCommonWord(String str)
	{
		List<String> commonStr = new ArrayList<String>();
		commonStr.add("file");
		if(str == null) return true;
		if(str.equals("")) return true;
		str = str.toLowerCase();
		if(str.length() > 2) 
		{
			if(commonStr.contains(str)) return true;
			return false;
		}
		return true;
	}

	//Equal return 2, similar return 1, not similar or common word return 0;
	//With Clipping Method, Still under development
	public static int getSimilarity(String word1, String word2)
	{
		if(word1 == null) return 0;
		if(word1.equals("")) return 0;
		word1 = word1.toLowerCase();
		word2 = word2.toLowerCase();
		if(isCommonWord(word1) || isCommonWord(word2)) return 0;
		if(word1.equals(word2)) return SAMESTRINGWEIGHT;
		if(word1.length() < word2.length())
		{
			String tmp = word1;
			word1 = word2;
			word2 = tmp;
		}
		if(word1.contains(word2)) return 1;

		if(word1.length() > 10)
		{
			String wordStem1 = word1.substring(0, word1.length() - 7);
			int minLength = wordStem1.length();
			if(minLength > word2.length()) minLength = word2.length();
			String wordStem2 = word2.substring(0, minLength);
	//		System.out.println(wordStem1);
	//		System.out.println(wordStem2);
			if(wordStem1.contains(wordStem2)) return 1;
	//		if(wordStem2.contains(wordStem1)) return 1;
		}
		else if(word1.length() > 6)
		{
			String wordStem1 = word1.substring(0, word1.length() - 4);
			int minLength = wordStem1.length();
			if(minLength > word2.length()) minLength = word2.length();
			String wordStem2 = word2.substring(0, minLength);
	//		System.out.println(wordStem1);
	//		System.out.println(wordStem2);
			if(wordStem1.contains(wordStem2)) return 1;
	//		if(wordStem2.contains(wordStem1)) return 1;
		}
		else if(word1.length() > 4)
		{
	//		System.out.println("length > 4");
			String wordStem1 = word1.substring(0, word1.length() - 2);
			int minLength = wordStem1.length();
			if(minLength > word2.length()) minLength = word2.length();
			String wordStem2 = word2.substring(0, minLength);
			if(wordStem1.equals(wordStem2)) return 1;
			
			//deals with prefix
			String prefixStem1 = word1.substring(2);
	//		System.out.println("deals with prefix");
			if(prefixStem1.contains(word2) || word2.contains(prefixStem1)) return 1;
		}


		return 0;
	}

	
	public String getInvertedIndex() throws JSONException
	{
		return csvFilesObject.getInvertedIndex() +"\n" + articleFileObj.getInvertedIndex();
	}
	
	public String getEdges() throws JSONException
	{
		return csvFilesObject.getEdges() +"\n" + articleFileObj.getEdges();
	}
	
	public void generateAllEdges()
	{
		if(allEdges == null) allEdges = new ArrayList<Map<String, Integer>>();
		allEdges.addAll(articleFileObj.getEdgeList());
		allEdges.addAll(csvFilesObject.getEdgeList());
	}
	
	public String getEdgesInOne() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("EDGE", allEdges);
		return jsonKVPair.toString();
	}
	
	public String getInvertedIndexInOne() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("INVERTED_INDEX", totalIIP);
		return jsonKVPair.toString();
	}
	
	public String getInvertedIndexInBSON()
	{
		BasicBSONObject jsonKVPair = new BasicBSONObject();
		jsonKVPair.put("INVERTED_INDEX", totalIIP);
		return jsonKVPair.toString();
	}
	
	public Map<String, List<Integer>> getInvertedIndexInMap()
	{
		return totalIIP;
	}
	
	public Map<String, Map<String, Integer>> getLinkerInMap()
	{
		return linker;
	}
	
	public List<Map<String, Integer>>  getEdgesInMap()
	{
		return allEdges;
	}
	
	public String getLinker() throws JSONException
	{
		JSONObject jsonKVPair = new JSONObject();
		jsonKVPair.put("LINKER", linker);
		return jsonKVPair.toString();
	}
	
	public String getKVPair() throws JSONException
	{
		return articleFileObj.getKVPair() + "\n" + csvFilesObject.getKVPair();
	}
	
	public void setKVPair()
	{
//		if(kvPairs == null) kvPairs = new HashMap<Integer, Map<String, String>>();
		kvPairs.putAll(articleFileObj.getKVPairInMap());
		kvPairs.putAll(csvFilesObject.getKVPairInMap());
	}
	
	public Map<Integer, Map<String, String>> getKVPairInMap()
	{
		Map<Integer, Map<String, String>> tempMap = 
				csvFilesObject.getKVPairInMap();
		tempMap.putAll(articleFileObj.getKVPairInMap());
		return tempMap;
	}
	
	public String getKVPairInBSON()
	{
		Map<Integer, Map<String, String>> tempMap = 
				csvFilesObject.getKVPairInMap();
		tempMap.putAll(articleFileObj.getKVPairInMap());
		BSONObject bsonKVPair = new BasicBSONObject();
		bsonKVPair.put("KVPAIR", tempMap);
		return bsonKVPair.toString();
	}
	
	public String getKVPairs() throws JSONException
	{
		Map<Integer, Map<String, String>> tempMap = 
				csvFilesObject.getKVPairInMap();
		tempMap.putAll(articleFileObj.getKVPairInMap());
		JSONObject bsonKVPair = new JSONObject();
		bsonKVPair.put("KVPAIR", kvPairs);
		return bsonKVPair.toString();
	}
	
	public int getLastIndex()
	{
		return lastIndex;
	}
	
	public String getLastIndexInJSON()
	{
		Map<String, Integer> tempMap = new HashMap<String, Integer>();
		tempMap.put("LAST_INDEX", lastIndex);
		JSONObject jsonLastIndex = new JSONObject(tempMap);
		return jsonLastIndex.toString();
	}
	
	public void updataDatabyJSONArray(String data)
	{
		int index = lastIndex;
		JSONArray jsonArrayData = new JSONArray(data);
		JSONObject jsonTempObj = new JSONObject();
		JSONObject jsonInvIndex = new JSONObject();
		JSONObject jsonEdge = new JSONObject();
		JSONObject jsonKVPair = new JSONObject();
		JSONObject jsonLinker = new JSONObject();
		for(int i = 0; i < jsonArrayData.length(); i ++)
		{
			jsonTempObj = jsonArrayData.getJSONObject(i);
			if(jsonTempObj.has("INVERTED_INDEX")) jsonInvIndex = jsonTempObj;
			else if(jsonTempObj.has("EDGE")) jsonEdge = jsonTempObj; 
			else if(jsonTempObj.has("KVPAIR")) jsonKVPair = jsonTempObj; 
			else if(jsonTempObj.has("LINKER")) jsonLinker = jsonTempObj; 
		}
		Map<Integer, Map<String, String>> tempKVPairs = 
				new HashMap<Integer, Map<String, String>>();
	    Map<String, String> singleKVPair = 
				new HashMap<String, String>();
	//    System.out.println(jsonKVPair);
	    jsonKVPair = jsonKVPair.getJSONObject("KVPAIR");
		Set<String> kvPairKeys = jsonKVPair.keySet();
	//	System.out.println(jsonKVPair);
		for(String key: kvPairKeys)
		{
			singleKVPair = new HashMap<String, String>();
	//		System.out.println(jsonKVPair.getJSONObject(key));
			Set<String> tempKey = jsonKVPair.getJSONObject(key).keySet();
			for(String word: tempKey)
			{
				singleKVPair.put(word, jsonKVPair.getJSONObject(key).getString(word));
			}
			
			tempKVPairs.put(Integer.parseInt(key), singleKVPair);
			index ++;
		}
		
		kvPairs.putAll(tempKVPairs);
	//	System.out.println(kvPairs);
		lastIndex = kvPairs.size();
		

		Map<String, List<Integer>> tempInvInd = 
				new HashMap<String, List<Integer>>();
		List<Integer> singleIndexList = new ArrayList<Integer>();
		jsonInvIndex = jsonInvIndex.getJSONObject("INVERTED_INDEX");
		Set<String> invIndKeys = jsonInvIndex.keySet();
		for(String key: invIndKeys)
		{
			singleIndexList = new ArrayList<Integer>();
			for(int i = 0; i < jsonInvIndex.getJSONArray(key).length(); i ++)
			{
				singleIndexList.add(jsonInvIndex.getJSONArray(key).getInt(i));
			}
			tempInvInd.put(key, singleIndexList);
			index ++;
		}
		totalIIP.putAll(tempInvInd);
	//	System.out.println(tempInvInd);
		
		
		Map<Integer, Map<String, String>> tempEdges = 
				new HashMap<Integer, Map<String, String>>();
	    Map<String, String> singleEdge = 
				new HashMap<String, String>();
	//    System.out.println(jsonKVPair);
	    JSONArray jsonEdgeList = jsonEdge.getJSONArray("EDGE");
	    for(int i = 0; i < jsonEdgeList.length(); i ++)
	    {
	    	Map<String, Integer> tempEdge = new HashMap<String, Integer>();
	    	Set<String> tempSet = jsonEdgeList.getJSONObject(i).keySet();
	    	for(String key: tempSet)
	    	{
	    		tempEdge.put(key, jsonEdgeList.getJSONObject(i).getInt(key));
	    	}
	    	allEdges.add(tempEdge);
	    }

	}

}
