package search;

import org.json.JSONArray;
import org.json.JSONObject;

import tikaExt.MongoUploader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;
public class Search {
	public static final int  WEIGHT_TOTAL = 7;
	public static final int  EDGE_WEIGHT = WEIGHT_TOTAL - 1;
	public static final int  RANK_FACTOR = 10;
	public int maxDepth;
	public int upperLimit =30;
	public   Map<Integer,List<Integer>> thisPath ;
	public   Map<Integer,Map<Integer,List<Integer>>> totalPath;
	public   Map<Integer,List<Integer>> adjLists;
	public   Map<Integer,List<Integer>> weight;
	public   Map<Integer,Integer> totalScore; // sort and get sortedScoreMap
	
	//constructor
	Search(int maxDepth,Map<Integer,List<Integer>> adjLists,Map<Integer,List<Integer>> score){

		thisPath = new HashMap<Integer,List<Integer>>();
		totalPath = new HashMap<Integer,Map<Integer,List<Integer>>>();	
		totalScore=new HashMap<Integer,Integer>();
		this.maxDepth=maxDepth;
		this.adjLists=adjLists;
		this.weight=score;
	}
	
	public Map<Integer,Integer> getTotalScore()
	{
		return totalScore;
	}
	//recurrsively run dfs
	public void dfs_rec(int current_node,int depth,List<Integer> currentPath,int destination, int pathRank){

		 if(depth<maxDepth && pathRank < RANK_FACTOR){			 
			 if (current_node==destination)
			 {
				 int tempSize = thisPath.size();
				 currentPath.add(destination);
					thisPath.put(tempSize, currentPath);
					totalScore.put(tempSize, pathRank);	
			 }
			 else{
				 List<Integer> tempList = adjLists.get(current_node);
					for (int i = 0; i < tempList.size(); i ++){
						int w = tempList.get(i);
						int thisWeight = transformScore(weight.get(current_node).get(i));
						if (!currentPath.contains(w)){
							
							List<Integer> nextPath = new ArrayList<Integer> ();
							nextPath.addAll(currentPath);
							nextPath.add(current_node);
							dfs_rec(w,depth+1,nextPath,destination,pathRank + thisWeight );
						}
			 }

			}

		}
	}
	//user interface
	public void userSearch(int start,int end){
		List <Integer> temp=new ArrayList<Integer>();
		dfs_rec(start,0,temp,end, 0);
	}
	//get edge from parser
	public static Map<Integer,List<Integer>> getAdjLists(JSONArray edge){
		
		Map<Integer,List<Integer>> adjLists = new HashMap<Integer,List<Integer>>();
		for (int i =0;i<100;i++){
			List<Integer> temp = new ArrayList<Integer>();
		}
		
	//read edge
		for (int i=0;i<edge.length();i++){
			JSONObject edgeSlice =edge.getJSONObject(i);
			List<Integer> temp = new ArrayList<Integer>();
			List<Integer> temp1 = new ArrayList<Integer>();
			if(adjLists.get(edgeSlice.getInt("orgnode"))!=null){
				temp=adjLists.get(edgeSlice.getInt("orgnode"));	
			}
			temp.add(edgeSlice.getInt("destnode"));
			adjLists.put(edgeSlice.getInt("orgnode"), temp);
			if(adjLists.get(edgeSlice.getInt("destnode"))!=null){
				temp1=adjLists.get(edgeSlice.getInt("destnode"));	
			}
			temp1.add(edgeSlice.getInt("orgnode"));
			adjLists.put(edgeSlice.getInt("destnode"), temp1);
		}
		return adjLists;
		
	}
	
	public static Map<Integer,List<Integer>> getLinkers(JSONArray edge){
		Map<Integer,List<Integer>> links = new HashMap<Integer,List<Integer>>();

		for (int i=0;i<edge.length();i++){
			JSONObject edgeSlice =edge.getJSONObject(i);
		List<Integer> temp = new ArrayList<Integer>();
		List<Integer> temp1 = new ArrayList<Integer>();
		if(links.get(edgeSlice.getInt("orgnode"))!=null){
			temp=links.get(edgeSlice.getInt("orgnode"));	
		}
		if (!edgeSlice.has("weight")){
			temp.add(EDGE_WEIGHT);
		}
		else{
			temp.add(edgeSlice.getInt("weight"));
		}
		links.put(edgeSlice.getInt("orgnode"), temp);
		
		if(links.get(edgeSlice.getInt("destnode"))!=null){
			temp1=links.get(edgeSlice.getInt("destnode"));	
		}
		if (!edgeSlice.has("weight")){
			temp1.add(EDGE_WEIGHT);
		}
		else{
			temp1.add(edgeSlice.getInt("weight"));
		}
		links.put(edgeSlice.getInt("destnode"), temp1);
		
		}
		return links;
		
	}
	
	
	public JSONObject transToString(int node,JSONObject kvPair){
		
		return kvPair.getJSONObject(Integer.toString(node));
		
	}
	
	public  String convertTostring(Map<Integer,List<Integer>> changeResult,JSONObject KVPair) throws IOException{
		JSONObject kvPair= new JSONObject();
		kvPair = KVPair.getJSONObject("KVPAIR");
		JSONArray ouputStruct=new JSONArray();
		JSONObject listString = new JSONObject();
		String output = "";
		int count = 0;
    	for (int i : totalScore.keySet())
    	{

			JSONArray containString = new JSONArray();
    		
    		List<Integer>temp1 =new ArrayList<Integer>();
    		temp1 = changeResult.get(i);
    		
    		containString= new JSONArray();
    		for  (int j=0;j<temp1.size();j++ )
    		{
    			
    			JSONObject stringLike;
    			stringLike = transToString(temp1.get(j),kvPair);		
    			containString.put(stringLike);
    		}
    	//	if (listString.length()<upperLimit){
    		listString.put("A_SCORE", totalScore.get(i));
    		listString.put("Z_PATH",containString);
    	//	}
    		output += listString.toString() + "<br/>";
    		ouputStruct.put(listString);
    		count ++;
    		if(count > upperLimit) return output;
    		
    	}

    	return output;

	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map )
	{
	    Map<K, V> result = new LinkedHashMap<>();
	    Stream<Map.Entry<K, V>> st = map.entrySet().stream();
	
	    st.sorted( Map.Entry.comparingByValue() )
	        .forEachOrdered( e -> result.put(e.getKey(), e.getValue()) );
	
	    return result;
	}
	
	public int transformScore(int weight)
	{
		return WEIGHT_TOTAL - weight;
	}
	
	public Map<Integer, List<Integer>> getPath()
	{
		return thisPath;
	}
	
	public static String readData (String userinput,int depth) throws IOException{
		
		String Mongo=MongoUploader.getParsedDataFromDB();
		Map<Integer,List<Integer>> adjLists = new HashMap<Integer,List<Integer>>();
		Map<Integer,List<Integer>> weight = new HashMap<Integer,List<Integer>>();
		JSONArray tempJSON = new JSONArray(Mongo);
		JSONObject indexPair = null;JSONArray edge = null;JSONObject ja3 = new JSONObject();JSONObject linkerPair = null;int lastIndex;
    	for(int i = 0; i < tempJSON.length(); i ++)
    	{
    		if (tempJSON.getJSONObject(i).has("INVERTED_INDEX")){
    			indexPair = tempJSON.getJSONObject(i).getJSONObject("INVERTED_INDEX");
    		}
    		if (tempJSON.getJSONObject(i).has("EDGE")){
    			 edge = tempJSON.getJSONObject(i).getJSONArray("EDGE");
    		}
    		if (tempJSON.getJSONObject(i).has("KVPAIR")){
    			 ja3 = tempJSON.getJSONObject(i);
    		}
    		if (tempJSON.getJSONObject(i).has("LINKER")){
    			linkerPair = tempJSON.getJSONObject(i).getJSONObject("LINKER");
    		}
    		if (tempJSON.getJSONObject(i).has("LAST_INDEX")){
    			 lastIndex = tempJSON.getJSONObject(i).getInt("LAST_INDEX");
    		}
    	}
    	Iterator e = linkerPair.keys();
		JSONArray linker2 =null;
		linker2=edge;

    	while(e.hasNext()){
    		String key =(String) e.next();
    		linker2.put(linkerPair.get(key));
    	}

		adjLists= getAdjLists(linker2);
		weight=getLinkers(linker2);
		Search a = new Search (depth,adjLists,weight);
		String start; String end;
		start = userinput.split(" ")[0];
		end = userinput.split(" ")[1];
		JSONArray beginSearch = indexPair.getJSONArray(start);
		JSONArray endSearch = indexPair.getJSONArray(end);
		for (int i=0;i<beginSearch.length();i++){
			
			for (int j=0;j<endSearch.length();j++){
				a.userSearch(Integer.parseInt(beginSearch.get(i).toString()),Integer.parseInt(endSearch.get(j).toString()));
				}
			
			}		
		Map<Integer, Integer> tempTotalScore = a.getTotalScore();
		a.totalScore = sortByValue(tempTotalScore);
	//	String newTemp ="";
		
		return a.convertTostring(a.thisPath,ja3);

	}
	
	
	public static void main(String[] args) throws IOException {
		
		
		
		try {
		String filePath = "PATH.txt";
		File file1 = new File(filePath);
		if (file1.exists()) 
		{  
          //  System.out.println("Exist");  
        } else {  
          //  System.out.println("Not Exist");  
			file1.createNewFile();

        } 
		BufferedWriter output = new BufferedWriter(new FileWriter(file1));  
        
		output.write(readData("purpose testdoc.docx",10));
        output.close(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println(readData("purpose testdoc.docx",10));

	}

}





